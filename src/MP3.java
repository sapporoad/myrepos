/**
 * Created by Sapporo on 17.07.15.
 */

/**
 *Тема -таск тебе разузнать про воспроизводсчтво звуков в джава,
 * написать класс который ловит в конструктор имя файла и при вызове метода старт проигывает.
 * сеттер для файла нового не забудь
 * через коллекцию чтобы подгружались все звуки, и по номеру включались
 */

//MP3 без сторонних библиотек нехотели вопроизводиться

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.*;
import javazoom.jl.player.advanced.AdvancedPlayer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class MP3 implements Runnable {
    private AdvancedPlayer ap;
    private InputStream instream;
    private AudioDevice device;
    private boolean released = false;
    private boolean playing = false;
    private ArrayList<String> mp3list = new ArrayList<String>();

    //сеттер для АррайЛиста, в который запихиваются все названия звуков
    public void setMp3list(String fileName){
        mp3list.add(fileName);
    }

    //Метод с помощью которого создается плеер, в который подгружается один из звуков. Входной параметр № звука из листа
    public void player(int i){
        try{
            instream = new FileInputStream(mp3list.get(i));
            device = new JavaSoundAudioDevice();
            ap = new AdvancedPlayer(instream, device);
            released = true;
        } catch(FileNotFoundException e){
            e.printStackTrace();
            released = false;
        } catch(Exception e){
            e.printStackTrace();
            released = false;
        }
    }

    //Метод для воспроизведения звука
    public void start(int x){
        try {
            player(x);
            if (released == true) {
                ap.play();
                playing = true;
            }
        } catch (JavaLayerException e) {
            e.printStackTrace();
            playing = true;
        }
        ap.close();
    }

    //Конструктор в который передается № трека, который нужно воспроизвести. Тут я и заполнил лист, хотя это можно
    //будет сделать в классе запуска программы
    public MP3(int numberSound){
        setMp3list("sources/Sounds/Eminem - Almost Famous.mp3");
        setMp3list("sources/Sounds/hello2.mp3");
        start(numberSound);
    }

    //Мэйн добавил для себя, тестил
    public static void main(String[] args) {
        new MP3(0);
    }

    //отдельный поток
    @Override
    public void run() {

    }
}


/**
 * Created by Sapporo on 7/9/15.
 */

import javax.sound.sampled.*;
import java.io.*;
import java.util.ArrayList;

public class Music implements Runnable {

    private Clip clip;
    private boolean released = false;
    private boolean playing = false;
    private ArrayList<String> mp3list = new ArrayList<String>(100);

    //сеттер для АррайЛиста, в который запихиваются все названия звуков
    public void setMp3list(String fileName){
        mp3list.add(fileName);
    }
    //Конструктор, в нем заполнял лист (больше не где)
    public Music (int numberSound) {
        setMp3list("resources/Sounds/test6.wav");
        setMp3list("resources/Sounds/test5.wav");
        setMp3list("resources/Sounds/wav-2.wav");
        setMp3list("resources/Sounds/wav-3.wav");
        setMp3list("resources/Sounds/midi-2.mid");
        player(numberSound);
        play();
        start();
    }
    //Метод для создания плеера
    public void player(int i){
        try {
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File(mp3list.get(i)));
            clip = AudioSystem.getClip();
            clip.open(ais);
            clip.addLineListener(new Listener());
            released = true;
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
            released = false;
        }
    }

    //Метод воспроизведения звука
    public void play() {
        if (released) {
            clip.setFramePosition(0);
            clip.start();
            playing = true;
        }
    }
    //Вдруг стоп понадобится
    public void stop(){
        if (playing == true)
            clip.stop();
    }

    //Метод проверяет воспроизводится ли файл
    public void start() {
        if (!released) return;
        synchronized(clip) {
            try {
                while (playing) clip.wait();
            } catch (InterruptedException e) {e.printStackTrace();}
        }
    }
    //Слушатель для плеера
    private class Listener implements LineListener {
        public void update(LineEvent ev) {
            if (ev.getType() == LineEvent.Type.STOP) {
                playing = false;
                synchronized(clip) {
                    clip.notify();
                }
            }
        }
    }

    @Override
    public void run() {

    }

    public static void main(String[] args) {
        new Music(2);
    }
}